import wx
import webbrowser
import sys
import os
import time
import re
from upload import Uploader
from bs4 import BeautifulSoup

API_KEY = ''
ID_SCREENSHOT_OPTION = wx.NewId()
ID_SELECTIVE_OPTION = wx.NewId()
ID_SETTINGS_OPTION = wx.NewId()

class Settings(wx.Frame):
    def __init__(self, parent=None, id=-1, title="", icon_class=None):
        wx.Frame.__init__(self, parent, id, title, size=(200, 100))
        
        self.panel = wx.Panel(self)
        self.panel.SetBackgroundColour("white")
        self.panel.Refresh()

        self.icon_class = icon_class

        self.path_label = wx.StaticText(self.panel, pos=(10, 10), label="Path:")
        self.save_path = wx.TextCtrl(self.panel, pos=(self.path_label.GetSize()[0]+10, self.path_label.GetSize()[1]/2))
        self.path_button = wx.Button(self.panel, pos=(160, self.path_label.GetSize()[1]/2), size=(20, self.save_path.GetSize()[1]), label="...")

        self.save_path.SetLabel(self.icon_class.save_path)

        self.done_button = wx.Button(self.panel, pos=(105, 35), label="Done", style=wx.EXPAND)
        self.done_button.Bind(wx.EVT_LEFT_DOWN, self.OnDone)

        self.path_button.Bind(wx.EVT_LEFT_DOWN, self.OnMouseDown)

    def OnDone(self, event):
        if os.path.exists(self.save_path.GetValue()):
            self.icon_class.save_path = self.save_path.GetValue()
            self.Close()
        else:
            wx.MessageBox("Path doesn't exist", "Path error", style=wx.ICON_INFORMATION)

    def OnMouseDown(self, event):
        dirDialog = wx.DirDialog(self.panel, "Select path", "", wx.DD_CHANGE_DIR)
        result = dirDialog.ShowModal()

        if result == wx.ID_OK:
            self.icon_class.save_path = dirDialog.GetPath()
            self.save_path.SetLabel(dirDialog.GetPath())

class SelectableFrame(wx.Frame):

    c1 = None
    c2 = None

    def __init__(self, parent=None, id=-1, title="", icon_class=None):
        wx.Frame.__init__(self, parent, id, title, size=wx.DisplaySize(), style=wx.NO_BORDER)

        self.icon_class = icon_class

        self.panel = wx.Panel(self, size=self.GetSize())

        self.panel.Bind(wx.EVT_MOTION, self.OnMouseMove)
        self.panel.Bind(wx.EVT_LEFT_DOWN, self.OnMouseDown)
        self.panel.Bind(wx.EVT_LEFT_UP, self.OnMouseUp)
        self.panel.Bind(wx.EVT_PAINT, self.OnPaint)

        self.panel.Bind(wx.EVT_KEY_DOWN, self.OnKeyDown)

        self.SetCursor(wx.StockCursor(wx.CURSOR_CROSS))

        self.SetTransparent(30)

    def OnKeyDown(self, event):
        if event.GetKeyCode() == 27:
            self.close()

    def close(self):
        self.panel.Destroy()
    	self.Destroy()

    def OnMouseMove(self, event):
        if event.Dragging() and event.LeftIsDown():
            self.c2 = event.GetPosition()
            self.Refresh()

    def OnMouseDown(self, event):
        self.c1 = event.GetPosition()

    def OnMouseUp(self, event):
        self.SetCursor(wx.StockCursor(wx.CURSOR_ARROW))

        if self.c2.x - self.c1.x <= 0 and self.c2.y - self.c1.y <= 0:

            self.icon_class.x = self.c2.x
            self.icon_class.y = self.c2.y
            self.icon_class.width = self.c1.x-self.c2.x
            self.icon_class.height = self.c1.y-self.c2.y
        else:
            self.icon_class.x = self.c1.x
            self.icon_class.y = self.c1.y
            self.icon_class.width = self.c2.x - self.c1.x
            self.icon_class.height = self.c2.y - self.c1.y

        self.panel.SetTransparent(0)
        self.SetTransparent(0)

        self.icon_class.TakeSelectiveScreenshot()

    def OnPaint(self, event):
        if self.c1 is None or self.c2 is None: return

        dc = wx.PaintDC(self.panel)
        dc.SetPen(wx.Pen('black', 1))
        dc.SetBrush(wx.Brush(wx.Color(0, 0, 0), wx.TRANSPARENT))

        dc.DrawRectangle(self.c1.x, self.c1.y, self.c2.x - self.c1.x, self.c2.y - self.c1.y)

class Icon(wx.TaskBarIcon):
    def __init__(self, parent, icon, tooltip):
        wx.TaskBarIcon.__init__(self)
        self.SetIcon(icon, tooltip)
        self.parent = parent
        self.Bind(wx.EVT_TASKBAR_LEFT_DCLICK, self.OnLeftDClick)
        self.CreateMenu()

        self.save_path = os.path.join(os.path.expanduser("~"), "Desktop") 

        self.x = 0
        self.y = 0
        self.width = 0
        self.height = 0
        self.frame = None

    def CreateMenu(self):
        self.Bind(wx.EVT_TASKBAR_RIGHT_UP, self.OnPopup)
        self.menu = wx.Menu()
        
        self.screenshot = self.menu.Append(ID_SCREENSHOT_OPTION, '&Take Screenshot')
        self.Bind(wx.EVT_MENU, self.OnScreenshot, self.screenshot)

        self.selective_screenshot = self.menu.Append(ID_SELECTIVE_OPTION, '&Take Selective Screenshot')
        self.Bind(wx.EVT_MENU, self.OnSelectiveScreenshot, self.selective_screenshot)

        self.menu.AppendSeparator()
        self.settings = self.menu.Append(ID_SETTINGS_OPTION, '&Settings')
        self.Bind(wx.EVT_MENU, self.OnSettings, self.settings)

        self.menu.AppendSeparator()
        self.exit = self.menu.Append(wx.ID_EXIT, '&Exit')
        self.Bind(wx.EVT_MENU, self.OnClose, self.exit)

    def OnSettings(self, event):
        self.settings_frame = Settings(icon_class=self, title="Settings")
        self.settings_frame.Show(True)
        self.settings_frame.Centre()

    def OnClose(self, event):
        self.Destroy()

    def TakeSelectiveScreenshot(self):
        pattern = re.compile(r'screenshot_selection_([0-9]*).png')
        last_file = [files for files in os.listdir(self.save_path) if pattern.match(files)]
        numbers_of_file = [int(files.split("_")[-1].split(".")[0]) for files in last_file]
        next_number = max(numbers_of_file)+1 if last_file else 0

        screen = wx.ScreenDC()
        size = (self.width, self.height)
        bmp = wx.EmptyBitmap(size[0], size[1])
        mem = wx.MemoryDC(bmp)
        mem.Blit(0, 0, size[0], size[1], screen, self.x, self.y)
        del mem
        bmp.SaveFile('%s/screenshot_selection_%s.png' % (self.save_path, next_number), wx.BITMAP_TYPE_PNG)

        dlg = wx.MessageDialog(None, 'Upload to Imageshack?', 'Upload?', wx.YES_NO | wx.ICON_QUESTION)
        result = dlg.ShowModal()
        if result == wx.ID_YES:

            u = Uploader(dev_key=API_KEY)
            img = u.uploadFile('%s/screenshot_selection_%s.png' % (self.save_path, next_number))
            soup = BeautifulSoup(img)
            url = soup.image_link.get_text()

            os.system('echo ' + url + '| clip')
            wx.MessageBox('Copied to clipboard!', 'Complete', wx.OK | wx.ICON_INFORMATION)

    	dlg.Destroy()
        self.frame.Destroy()

    def OnSelectiveScreenshot(self, event):
        self.frame = SelectableFrame(icon_class=self, title="Screenshot Window")
        self.frame.Show(True)

    def OnScreenshot(self, event):
        pattern = re.compile(r'screenshot_([0-9]*).png')

        last_file = [files for files in os.listdir(self.save_path) if pattern.match(files)]
        numbers_of_file = [int(files.split("_")[-1].split(".")[0]) for files in last_file]
        next_number = max(numbers_of_file)+1 if last_file else 0

        screen = wx.ScreenDC()
        size = screen.GetSize()
        bmp = wx.EmptyBitmap(size[0], size[1])
        mem = wx.MemoryDC(bmp)
        mem.Blit(0, 0, size[0], size[1], screen, 0, 0)
        del mem
        bmp.SaveFile('%s/screenshot_%s.png' % (self.save_path, next_number), wx.BITMAP_TYPE_PNG)

        dlg = wx.MessageDialog(parent=None, message='Upload to Imageshack?', caption='Upload', style=wx.YES_NO | wx.ICON_QUESTION | wx.STAY_ON_TOP)
        result = dlg.ShowModal()
        if result == wx.ID_YES:

            u = Uploader(dev_key=API_KEY)
            img = u.uploadFile('%s/screenshot_%s.png' % (self.save_path, next_number))
            soup = BeautifulSoup(img)
            url = soup.image_link.get_text()


            os.system('echo ' + url + '| clip') 
            wx.MessageBox('Copied to clipboard!', 'Complete', wx.OK | wx.ICON_INFORMATION)
        dlg.Destroy()

    def OnPopup(self, event):
        self.PopupMenu(self.menu)

    def OnLeftDClick(self, event):
        if self.parent.IsIconized():
            self.parent.Iconize(False)
        if not self.parent.IsShown():
            self.parent.Show(True)
        self.parent.Raise()

app = wx.PySimpleApp()
frame = Icon(None, wx.Icon("icon.png", wx.BITMAP_TYPE_PNG), "Screenshoot")
app.MainLoop()